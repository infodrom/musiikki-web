<?php

class Template {
    const DIR = 'templates';

    protected $template;
    protected $templatePath;

    public static function html($path, array $data=array())
    {
	$template = new Template($path);
	return $template->render($data);
    }

    public function __construct($template)
    {
        $this->template = $template;
        $this->templatePath = __DIR__ . '/../' . static::DIR . '/' . $this->template . '.phtml';

        if (!is_readable($this->templatePath))
            throw new Exception("Template {$this->template} not found");
    }

    public function render(array $data=array())
    {
        foreach ($data as $key => $value)
            $$key = $value;

        ob_start(); // Output Buffer einschalten
        include($this->templatePath);
        $renderedText = ob_get_contents();
        ob_end_clean(); // Output Buffer beenden

        return $renderedText;
    }
}
