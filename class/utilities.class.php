<?php

class Utilities {

  public static function formatScript($path)
  {
    if (substr($path,-3) != '.js' || substr($path,-7) == '.min.js')
      return sprintf('<script type="text/javascript" src="%s"></script>', $path);

    $base = substr($path,0,-3);
    $minfile = $base  . '.min.js';

    if (!file_exists($minfile))
      return sprintf('<script type="text/javascript" src="%s"></script>', $path);

    if (filemtime($minfile) > filemtime($path))
      return sprintf('<script type="text/javascript" src="%s"></script>', $minfile);

    return sprintf('<script type="text/javascript" src="%s"></script>', $path);
  }

  public static function formatCSS($path)
  {
    if (substr($path,-4) != '.css' || substr($path,-8) == '.min.css')
      return sprintf('<link rel="stylesheet" href="%s">', $path);

    $base = substr($path,0,-4);
    $minfile = $base  . '.min.css';

    if (!file_exists($minfile))
      return sprintf('<link rel="stylesheet" href="%s">', $path);

    if (filemtime($minfile) > filemtime($path))
      return sprintf('<link rel="stylesheet" href="%s">', $minfile);

    return sprintf('<link rel="stylesheet" href="%s">', $path);
  }

}
