<?php

class AutoLoader {
  public static $loader = false;
  protected static $classPath = __DIR__;

  public static function init()
  {
    if (self::$loader === false)
      self::$loader = new self();
  }

  public function __construct()
  {
    spl_autoload_register(array($this, 'autoload'));
  }

  protected function autoload($class)
  {
    $path = self::$classPath . '/' . strtolower($class) . '.class.php';

    if (is_readable($path))
      require_once($path);
  }
}

AutoLoader::init();
