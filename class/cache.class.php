<?php

class Cache {
    public function __construct()
    {
	$cache = Config::main()->get('cache');

	if (is_null($cache))
	    throw new Exception('Cache file not found');

	$this->db = new PDO('sqlite:/'.$cache);
    }

    public function getRelativePath($id)
    {
	$media_dir = Config::main()->get('media_dir_A');

	$sql = sprintf("SELECT path FROM details WHERE id = %d", $id);
	$sth = $this->db->query($sql);
	if ($sth === false) return null;

	$row = $sth->fetchObject();
	$path = substr($row->PATH, strlen($media_dir)+1);
	return $path;
    }

    public function findTitles($keyword)
    {
	$media_dir = Config::main()->get('media_dir_A');
	$results = array('count' => 0, 'list' => []);
	$sql = sprintf("SELECT id, path, title, timestamp, size FROM details WHERE path LIKE %s AND mime IS NOT NULL",
		       $this->db->quote('%'.$keyword.'%'));

	$sth = $this->db->query($sql);
	if ($sth === false) return $results;

	$seen = [];
	while ($row = $sth->fetchObject()) {
	    $id = "{$row->TIMESTAMP}-{$row->SIZE}";
	    if (!array_key_exists($id, $seen)) {
		$results['count']++;
		$seen[$id] = true;
	    }
	    $section = '[empty]';
	    $path = substr($row->PATH, strlen($media_dir)+1);

            if (($pos = strrpos($path, '.')) > strlen($path)-10)
		$path = substr($path, 0, $pos);

	    if (($pos = strpos($path, '/')) < 20) {
	        $section = substr($path, 0, $pos);
		$path = substr($path, $pos+1);
	    }

	    if (!array_key_exists($section, $results['list']))
		$results['list'][$section] = [];

	    $item = ['path' => dirname($path), 'name' => basename($path), 'title' => $row->TITLE];
	    if (array_key_exists('playlist', $_SESSION))
		$item['id'] = $row->ID;
	    else
		$item['id'] = '';

	    $results['list'][$section][] = $item;
	}

	return $results;
    }
}