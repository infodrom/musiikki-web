<?php

class AJAX {
    protected static function callCommand($cmd, array $data=array())
    {
	if (($pipe = popen($cmd, 'w')) === false)
	    return false;

	foreach ($data as $value)
	    fwrite($pipe, $value . "\n");

	pclose($pipe);
	return true;
    }

    public static function getdatetimeAction()
    {
	return array('year' => date('Y'),
		     'month' => date('m'),
		     'day' => date('d'),
		     'hour' => date('H'),
		     'minute' => date('i'),
		     'second' => date('s'));
    }

    public static function datetimeAction()
    {
	$date = sprintf("%04d-%02d-%02d", $_POST['year'], $_POST['month'], $_POST['day']);
	$time = sprintf("%02d:%02d:%02d", $_POST['hour'], $_POST['minute'], $_POST['second']);

	$ok = self::callCommand("super musiikki-datetime", array($date, $time));

	return $ok;
    }

    public static function getwifiAction()
    {
	if (!file_exists('/etc/hostapd/hostapd.conf'))
	    return array();

	if (($f = fopen('/etc/hostapd/hostapd.conf', 'r')) === false)
	    return array();

	$essid = '';
	$password = '';

	while (($line = fgets($f)) !== false) {
	    $line = trim($line);
	    if (substr($line,0,1) == '#') continue;
	    if (!strlen($line)) continue;

	    $parts = explode('=', $line, 2);
	    if ($parts[0] == 'ssid')
		$essid = $parts[1];
	    elseif ($parts[0] == 'wpa_passphrase')
		$password = $parts[1];
	}

	fclose($f);

	return array('essid' => $essid,
		     'password' => $password);
    }

    public static function wifiAction()
    {
	$ok = self::callCommand("super musiikki-wifi", array($_POST['essid'], $_POST['password']));

	return $ok;
    }

    public static function getnetworkAction()
    {
	$name = NULL;
	$conf = '/etc/network/interfaces.d/uplink.conf';
	if (file_exists($conf)) {
	    $dest = readlink($conf);
	    $name = substr($dest, 9, -5);
	}

	$upstream = [];
	foreach (new DirectoryIterator(dirname($conf)) as $fileInfo) {
	    if (substr($fileInfo->getBasename(), 0, 9) != 'upstream_') continue;
	    $upstream[] = substr($fileInfo->getBasename('.conf'), 9);
	}
	sort($upstream);

	$ip = NULL;
	$lines = [];
	$f = popen("ip -f inet addr show wlan0", 'r');
	while (($line = fgets($f)) !== false)
	    $lines[] = rtrim($line);
	pclose($f);
	if (count($lines)) {
	    if (preg_match('/inet (\d+\.\d+\.\d+\.\d+)\/.*/', $lines[1], $matches))
		$ip = $matches[1];
	}

	$networks = [];
	$f = popen("(sudo iwlist wlan0 scan; sudo iwlist wlan1 scan)|sort -u", 'r');
	while (($line = fgets($f)) !== false) {
	    if (preg_match('/ESSID:"(.*?)"/', $line, $matches))
		if (strlen($matches[1]))
		    $networks[] = $matches[1];
	}
	pclose($f);
	sort($networks);

	return ['name' => $name,
		'ip' => $ip,
		'networks' => $networks,
		'upstream' => $upstream];
    }

    public static function networkAction()
    {
	$ok = self::callCommand("super musiikki-wlan", array($_POST['wlan']));

	return $ok;
    }

    public static function essidAction()
    {
	if (!preg_match('/^[A-Za-z0-9_-]+$/', $_POST['name']))
	    return;

	$ok = self::callCommand("super musiikki-essid", array($_POST['name'], $_POST['essid'], $_POST['psk']));

	return $ok;
    }

    public static function getsharesAction()
    {
	return array('ro_name' => '\\\\' . $_SERVER['SERVER_ADDR'] .'\\'. Config::SHARE_READ,
		     'ro_user' => Config::SHARE_READ,
		     'ro_password' => '',
		     'rw_name' => '\\\\' . $_SERVER['SERVER_ADDR'] .'\\'. Config::SHARE_WRITE,
		     'rw_user' => Config::SHARE_WRITE,
		     'rw_password' => '');
    }

    public static function sharesAction()
    {
	$ok = self::callCommand("super musiikki-shares", array($_POST['ro_password'], $_POST['rw_password']));

	return $ok;
    }

    public static function getpasswdAction()
    {
	return array();
    }

    public static function passwdAction()
    {
	if (!strlen($_POST['password'])) return false;

	if (($f = fopen(Config::main()->get('password'), 'w')) === false) return false;

	fwrite($f, sprintf("admin:%s\n", $_POST['password']));
	fclose($f);

	return true;
    }

    public static function gethaltAction()
    {
	return array();
    }

    public static function haltAction()
    {
	$ok = self::callCommand("super musiikki-wlan", array(''));
	$ok = self::callCommand("super musiikki-halt");

	return $ok;
    }

    public static function getrescanAction()
    {
	$mtime = filemtime(Config::main()->get('cache'));

	$result = array('modified' => date ("d. F Y H:i:s", $mtime),
			'running' => $mtime > time()-10,
			);

	return $result;
    }

    public static function rescanstatusAction()
    {
	$cmd = "tail -n 1 /var/log/minidlna.log  2>&1";

	$file = '';
	$line = shell_exec($cmd);
	if ($line !== false && !is_null($line)) {
	    $search = "info: Scanning /media/music/music/";
	    $pos = strpos($line, $search);
	    if ($pos !== false)
		$file = substr($line, $pos + strlen($search));
	}

	$mtime = filemtime(Config::main()->get('cache'));

	$result = array('file' => $file,
			'end' => $mtime < time()-60);

	return $result;
    }

    public static function rescanAction()
    {
	$mtime = filemtime(Config::main()->get('cache'));
	if ($mtime > time()-10)
	    return array('info' => 'Rescan läuft gerade und kann nicht erneut getriggert werden.');

	if (($f = fopen(Config::main()->get('rescan_file'), 'w')) !== false) {
	    fwrite($f, time());
	    fclose($f);
	    return true;
	}

	return false;
    }

    public static function getsearchAction()
    {
	return array();
    }

    public static function searchAction()
    {
	$cache = new Cache();

	$list = $cache->findTitles($_POST['keyword']);

	return ['results' => $list,
		'playlist' => $_SESSION['playlist']];
    }

    public static function getresultsAction()
    {
	return array();
    }

    public static function playlistSelectAction()
    {
	$_SESSION['playlist'] = $_POST['name'];
    }

    public static function getplaylistsAction()
    {
	$playlists = new Playlists();
	$selected = null;
	if (array_key_exists('playlist', $_SESSION))
	    $selected = $_SESSION['playlist'];

	return ['list' => $playlists->getList(),
		'selected' => $selected];
    }

    public static function playlistAction()
    {
	$playlists = new Playlists();

	return ['title' => $_POST['name'],
		'list' => $playlists->getContents($_POST['name'])];
    }

    public function playlistNewAction()
    {
	$playlists = new Playlists();
	$playlists->create($_POST['name']);
    }

    public function playlistAddAction()
    {
	$cache = new Cache();
	$playlists = new Playlists();

	if (!array_key_exists('playlist', $_SESSION))
	    return;

	$playlists->add($_SESSION['playlist'], $cache->getRelativePath($_POST['id']));
    }
}
