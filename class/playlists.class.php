<?php

class Playlists {
    const EMPTYSTRING = 'not empty';

    public function getList()
    {
	$dir = Config::main()->get('media_dir_A') . '/playlists';
	$list = [];

	foreach (new DirectoryIterator($dir) as $fileInfo) {
	    if ($fileInfo->isDot()) continue;
	    if ($fileInfo->isDir()) continue;
	    if ($fileInfo->getExtension() != 'm3u') continue;

	    $lines = preg_split('/\r?\n/', file_get_contents($fileInfo->getPathname()));
	    if ($lines[0] == static::EMPTYSTRING) {
		$count = 0;
	    } else {
		$count = count($lines) - 1;
	    }

	    $list[] = ['name' => $fileInfo->getBasename('.m3u'), 'count' => $count];
	}

	usort($list, function($a, $b){return strcmp($a['name'], $b['name']);});
	return $list;
    }

    public function getContents($name)
    {
	$dir = Config::main()->get('media_dir_A') . '/playlists';
	$list = [];

	$path = $dir . '/' . $name . '.m3u';
	if (file_exists($path)) {
	    $content = explode("\n", file_get_contents($path));
	    foreach ($content as $path) {
		if (empty(trim($path))) continue;
		if ($path == static::EMPTYSTRING) continue;

		$fname = basename($path);

		if (($pos = strrpos($fname, '.')) > strlen($fname)-10)
		    $fname = substr($fname, 0, $pos);
		$list[] = $fname;
	    }	    
	}

	return $list;
    }

    public function add($name, $path)
    {
	$playlist = Config::main()->get('media_dir_A') . '/playlists/' . $name . '.m3u';
	if (!file_exists($playlist)) return;

	$content = explode("\n", file_get_contents($playlist));
	$found = false;
	foreach ($content as $line)
	    if ($line == $path)
		$found = true;

	if (!$found)
	    $content[] = $path;

	if (($f = fopen($playlist.'.new', 'w')) !== false) {
	    foreach ($content as $line) {
		if (empty(trim($line))) continue;
		if ($line == static::EMPTYSTRING) continue;
		fwrite($f, $line . "\n");
	    }
	    fclose($f);
	    rename($playlist.'.new', $playlist);
	}
	
    }

    public function create($name)
    {
	$playlist = Config::main()->get('media_dir_A') . '/playlists/' . $name . '.m3u';
	if (file_exists($playlist)) return;

	if (($f = fopen($playlist, 'w')) !== false) {
	    fwrite($f, static::EMPTYSTRING . "\n");
	    
	    fclose($f);
	}
    }
}
