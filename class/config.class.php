<?php

class Config {
  const PATH = '/etc/musiikki.conf';
  const PATH_DLNA = '/etc/minidlna.conf';
  const SHARE_READ = 'musicread';
  const SHARE_WRITE = 'musicwrite';

  private static $main = false;

  private $values = array();

  public static function main()
  {
    if (self::$main === false)
      self::$main = new Config(Config::PATH);

    return self::$main;
  }

  private function __construct($path)
  {
    $this->loadValues($path);
    $this->loadMiniDLNA(static::PATH_DLNA);
  }

  private function loadValues($path)
  {
    if (!is_readable($path))
      throw new Exception('Cannot open config file ' . $path);

    $f = fopen($path, 'r');
    if ($f === false)
      throw new Exception('Cannot open config file ' . $path);

    while (($line = fgets($f)) !== false) {
      $line = trim($line);
      if (substr($line,0,1) == '#') continue;
      if (!strlen($line)) continue;

      $parts = explode('=', $line, 2);
      $this->values[$parts[0]] = $parts[1];
    }

    fclose($f);
  }

  private function loadMiniDLNA($path)
  {
    if (!is_readable($path))
      throw new Exception('Cannot open config file ' . $path);

    $f = fopen($path, 'r');
    if ($f === false)
      throw new Exception('Cannot open config file ' . $path);

    while (($line = fgets($f)) !== false) {
      $line = trim($line);
      if (substr($line,0,1) == '#') continue;
      if (!strlen($line)) continue;
      if (substr($line,0,10) != 'media_dir=') continue;

      $parts = explode('=', $line, 2);
      $details = explode(',', $parts[1], 2);
      $this->values[$parts[0].'_'.$details[0]] = $details[1];
    }

    fclose($f);
  }

  public function get($name)
  {
    if (!array_key_exists($name, $this->values))
      return NULL;

    return $this->values[$name];
  }
}
