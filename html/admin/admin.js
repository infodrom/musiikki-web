function submit_datetime() {
    $.post('index.php',
	   'action=datetime&' + $('div.w3-container#datetime form').serialize(),
	   function(data){
	       if (data)
		   response('Aktuelle Uhrzeit und Datum gesetzt');
	   });

    return false;
}

function submit_wifi() {
    $.post('index.php',
	   'action=wifi&' + $('div.w3-container#wifi form').serialize());
    response('WLAN neu konfiguriert.<br>Sie müssen sich mit neuem Paßwort im WLAN anmelden.');

    return false;
}

function submit_shares() {
    $.post('index.php',
	   'action=shares&' + $('div.w3-container#shares form').serialize(),
	   function(data){
	       if (data)
		   response('Passwörter für die Netzwerk-Laufwerke gesetzt');
	   });

    return false;
}

function submit_rescan() {
    $.post('index.php',
	   'action=rescan',
	   function(data){
	       if (data) {
		   if (data.info)
		       response(data.info);
		   else
		       response('Rescan wurde getriggert.<br>Die Medienliste wird neu aufgebaut.');
	       }
	   });

    return false;
}

function submit_passwd() {
    $.post('index.php',
	   'action=passwd&' + $('div.w3-container#passwd form').serialize());
    response('Admin-Passwort neu gesetzt.<br>Sie müssen sich erneut anmelden.');

    return false;
}

function submit_halt() {
    $.post('index.php',
	   'action=halt',
	   function(data){
	       if (data)
		   response('Der Server kann jetzt vom Strom getrennt und eingepackt werden.', true);
	   });

    return false;
}

function network_process(data)
{
    if (data.ip === null)
	$('#network h1:nth-child(1) b').text('WLAN.');
    else
	$('#network h1:nth-child(1) b').text(data.ip);

    $('#network select[name="wlan"]').empty();
    var option = $('<option value="">-- select --</option>');
    $('#network select[name="wlan"]').append(option);
    for (var i=0; i < data.upstream.length; i++) {
	var option = $('<option value="'+data.upstream[i]+'">'+data.upstream[i]+'</option>');
	if (data.name !== null && data.name == data.upstream[i]) option.attr('selected','selected');
	$('#network select[name="wlan"]').append(option);
    }

    $('#network select[name="essid"]').empty();
    var option = $('<option value="">-- select --</option>');
    $('#network select[name="essid"]').append(option);
    for (var i=0; i < data.networks.length; i++) {
	var option = $('<option value="'+data.networks[i]+'">'+data.networks[i]+'</option>');
	$('#network select[name="essid"]').append(option);
    }

    $('#network input[name="name"]').val('');
    $('#network input[name="psk"]').val('');
}

function submit_network()
{
    $.post('index.php',
	   'action=network&wlan=' + $('#network select[name="wlan"]').val());
    response('Netzwerk gespeichert.');

    return false;
}

function rescan_status()
{
    $('#rescan input[name="file"]').val('query');
    $.post('index.php',
	   'action=rescanstatus',
	   function(data){
	       $('#rescan input[name="file"]').val('file');
	   });
}

function rescan_process(data)
{
    if (data.running) {
	$('#rescan input[name="modified"]').val('');
	$('#rescan input[name="file"]').val('');
	$('#rescan #running').show();
	$('#rescan #button').hide();
	rescan_status();
    } else {
	$('#rescan input[name="modified"]').val(data.modified);
	$('#rescan #running').hide();
	$('#rescan #button').show();
    }
}

function submit_essid()
{
    if (!$('#network input[name="name"]').val().length) return false;
    if (!$('#network select[name="essid"]').val().length) return false;
    if (!$('#network input[name="psk"]').val().length) return false;

    $.post('index.php',
	   'action=essid&' + $('div.w3-container#network form#new_essid').serialize());
    response('Netzwerk gespeichert.');

    return false;
}

$(function(){
    register_callback('network', network_process);
    register_callback('rescan', rescan_process);
});
