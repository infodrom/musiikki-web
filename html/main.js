function toggle_section(event)
{
    $(this).parent().find('ul').toggle();
}

var waiting_stop = false;
var waiting_forward = true;
function results_waiting_stop()
{
    waiting_stop = true;
    if (waiting_forward)
	waiting_forward = false;
}

function results_waiting()
{
    var max = $(window).width() - 120;
    var step = 15;
    var hr = $('div#results hr');
    var margin = hr.css('marginLeft').replace('px','');
    if (margin.length)
	margin = parseInt(margin, 10);
    else
	margin = 0;

    if (margin == 0 && waiting_stop) {
	waiting_stop = false;
    } else {
	if (margin > max)
	    waiting_forward = false;
	else if (margin == 0)
	    waiting_forward = true;

	if (waiting_forward)
	    margin += step;
	else
	    margin -= step;

	hr.css('marginLeft', margin+'px');
	setTimeout(results_waiting,200);
    }
}
setTimeout(results_waiting_stop,20000);

function search_addto_playlist(event)
{
    var btn = $(this);
    console.log($(this).attr('data-id'));

    $.post('index.php',
	   {action: 'playlistAdd',
	    id: $(this).attr('data-id')},
	   function(data){
	       btn.hide();
	   });
}

function submit_search(e)
{
    var input = $(e).parent().find('#search_keyword');
    if (!input.val().length)
	return false;

    $('div#results h1').text('Results.');
    open_page('results');
    $('div#results div.w3-section').hide();
    results_waiting();

    $.post('index.php',
	   'action=search&keyword=' + encodeURI(input.val()),
	   function(data){
	       if (data) {
		   input.val('');
		   if (data.playlist != null && data.playlist.length) {
		       $('#results #playlist').find('b').text(data.playlist);
		       $('#results #playlist').show();
		   } else {
		       $('#results #playlist').hide();
		   }
		   $('div#results h1').text(data.results.count+' matches.');
		   var result = $('<ul class="section">');

		   for (section in data.results.list) {
		       var dom_li = $('<li class="section">');
		       var dom_div = $('<div class="section">');
		       dom_div.append($('<strong class="w3-round">').text(section).click(toggle_section));

		       var dom_ul = $('<ul class="files">').hide();
		       for (var i=0; i < data.results.list[section].length; i++) {
			   var item = $('<li>');
			   if (i % 2 == 1)
			       item.addClass('bg');
			   item.append($('<div class="it">').html(data.results.list[section][i].title));
			   item.append($('<div class="ip">').html(data.results.list[section][i].path));
			   item.append($('<div class="in">').html(data.results.list[section][i].name));

			   if (data.results.list[section][i].id.length) {
			       var plus = $('<div class="plus w3-round">').text('+');
			       plus.click(search_addto_playlist);
			       plus.attr('data-id', data.results.list[section][i].id);
			       item.append(plus);
			   }

			   dom_ul.append(item);
		       }
		       dom_div.append(dom_ul);

		       dom_li.append(dom_div);
		       result.append(dom_li);
		   }

	           $('div#results div.w3-section').empty().append(result);
	           $('div#results div.w3-section').show();
	           // $('div#results div.waiting').hide();
		   results_waiting_stop();
	       }
	   });

    return false;
}

function playlists_select(event)
{
    var name = $(this).attr('data-name');
    $.post('index.php',
	   {action: 'playlistSelect',
	    name: name},
	   function(data){
	       $.post('index.php',
		      {action: 'playlist',
		       name: name},
		      playlist_process);
	   });
}

function playlists_process(data)
{
    var ul = $('<div class="playlists">');
    for (var i=0; i < data.list.length; i++) {
	var li = $('<div class="w3-round item">');
	li.attr('data-name', data.list[i].name);
	li.click(playlists_select);
	li.append($('<span style="float:left;">'+data.list[i].name+'</span>'));
	li.append($('<span style="float:right;">'+data.list[i].count+'</span>'));
	li.append($('<div style="clear:both;"></div>'));
	if (data.selected && data.selected == name)
	    li.addClass('selected');
	ul.append(li);
    }
    $('div#playlists div.w3-section').empty().append(ul);
}

function playlist_process(data)
{
    $('div#playlist #title').text(data.title+'.');
    if (data.list.length == 0) {
	var ol = $('<p>');
	ol.text('Playlist ist noch leer.');
    } else {
	var ol = $('<ol class="playlist">');
	for (var i=0; i < data.list.length; i++) {
	    var name = data.list[i];
	    var li = $('<li>');
	    li.text(name);
	    ol.append(li);
	}
    }
    $('div#playlist div.w3-section').empty().append(ol);
    open_page('playlist');
}

function submit_playlist()
{
    $.post('index.php',
	   {action: 'playlistNew',
	    name: $('div#playlists input#name').val()},
	   function(data){
	       $('div#playlists input#name').val('');
	       open_page('playlists');
	   });
}


$(function(){
    register_callback('playlists', playlists_process);
});
