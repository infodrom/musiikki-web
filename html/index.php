<?php
require_once(__DIR__.'/../class/autoloader.class.php');

session_name('MUSIIKKI');
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $template = new Template('main');
    echo $template->render([]);
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $method = $_POST['action'] . 'Action';

    header('Content-type: application/json; charset=UTF-8');
    $data = AJAX::$method($_POST);
    $return = json_encode($data);

    if ($return === false) {
	error_log('Return ' . var_export($data,true));
	$return = json_encode(array('status' => false, 'error' => 'Rückgabedaten können nicht kodiert werden.'));
    }

    echo $return;
}