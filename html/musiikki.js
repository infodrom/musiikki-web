// Script to open and close sidebar
var page_callbacks = [];
function w3_open()
{
    $('#mySidebar').css('display', 'block');
    $('#myOverlay').css('display', 'block');
}

function w3_close()
{
    $('#mySidebar').hide();
    $('#myOverlay').hide();
}

function register_callback(page, callback)
{
    page_callbacks[page] = callback;
}

function open_page(page) {
    w3_close();
    $('div.w3-container').hide();

    if ($('div.w3-container#'+page + '[data-fetch="true"]').length)
	$.post('index.php',
	       'action=get'+page,
	       function(data){
		   if (typeof data == 'object') {
		       if (page in page_callbacks) {
			   page_callbacks[page](data);
		       } else {
			   for (name in data) {
			       obj = $('div.w3-container#'+page + ' [name="'+name+'"]');
			       if (obj.length)
				   obj.val(data[name]);
			   }
		       }
		   }
	       });
    $('div.w3-container#'+page).show();
}

function response(text, stay)
{
    $('div.w3-container').hide();
    $('div.w3-container#response #msg').html(text);
    $('div.w3-container#response').show();
    if (!stay)
	setTimeout(function(){
	    open_page('home');
	}, 5000);
}

$(function(){
    $('div.w3-container').hide();
    $('div.w3-container#home').show();
});
